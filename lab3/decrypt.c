/**
 * Author:    Jan Mraz
 * Created:   25.03.2020
 **/
#include <stdlib.h>
#include <openssl/evp.h>
#include <string.h>

#define FREAD_BUFFER_LENTH 2048

int main(int argc, char ** argv) {
    int res;
    unsigned char buf[FREAD_BUFFER_LENTH];
    uint32_t totalSize = 0;
    uint32_t newSize = 0;
    int bytesRead = 0;
    int bytesWritten = 0;
    int currPos = 0;
    int bytesRemainingToOffset = 0;
    int bytesToRead = 0;
    uint32_t pixelsOffset = 0;
    FILE *inFile = stdin;
    FILE *outFile = stdout;


    unsigned char key[EVP_MAX_KEY_LENGTH] = "SomeSuperDuperKey";  // klic pro sifrovani
    unsigned char iv[EVP_MAX_IV_LENGTH] = "a vector";  // inicializacni vektor
    const EVP_CIPHER *cipher;

    OpenSSL_add_all_ciphers();

    /* sifry i hashe by se nahraly pomoci OpenSSL_add_all_algorithms() */
    cipher = EVP_get_cipherbyname(argv[1]);
    if(!cipher) {
        printf("Sifra %s neexistuje.\n", argv[1]);
        exit(1);
    }

    EVP_CIPHER_CTX *ctx; // context structure
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL) exit(2);

    // Copy first 2 bytes - check magic numbers
    bytesRead = fread(buf,  sizeof(unsigned char), 2, inFile);
    if (bytesRead < 2 || buf[0] != 'B' || buf[1] != 'M') exit(3);
    bytesWritten = fwrite(buf,  sizeof(unsigned char), bytesRead, outFile);
    if (bytesWritten < 2) exit(4);

    // Copy second 4 bytes - check total size
    bytesRead = fread(&totalSize,  sizeof(uint32_t), 1, inFile);
    if (bytesRead < 1) exit(5);
    bytesWritten = fwrite(&totalSize,  sizeof(uint32_t), bytesRead, outFile);
    if (bytesWritten < 1) exit(6);

    // Copy next 4 bytes -- reserved bytes
    bytesRead = fread(buf,  sizeof(unsigned char), 4, inFile);
    if (bytesRead < 4) exit(7);
    bytesWritten = fwrite(buf,  sizeof(unsigned char), bytesRead, outFile);
    if (bytesWritten < 4) exit(8);

    // Copy the offset 4 bytes
    bytesRead = fread(&pixelsOffset, sizeof(uint32_t), 1, inFile);
    if (bytesRead < 1) exit(9);
    bytesWritten = fwrite(&pixelsOffset, sizeof(uint32_t), 1, outFile);
    if (bytesWritten < 1) exit(10);

    // check if offset is within the file
    if(pixelsOffset > totalSize || pixelsOffset < 14){
        exit(11);
    }

    // offset - 14 bytes already read and written
    bytesRemainingToOffset = pixelsOffset - 14;

    newSize = 14;

    // Copy everything up until the picture data
    while (bytesRemainingToOffset > 0) {
        bytesToRead = bytesRemainingToOffset < FREAD_BUFFER_LENTH ? bytesRemainingToOffset : FREAD_BUFFER_LENTH;

        bytesRead = fread(buf, sizeof(unsigned char), bytesRemainingToOffset, inFile);
        bytesWritten = fwrite(buf, sizeof(unsigned char),bytesRemainingToOffset, outFile);
        if (bytesWritten < bytesToRead) exit(12);

        newSize += bytesWritten;
        bytesRemainingToOffset -= bytesWritten;
    }


    int tmpLength = 0;
    unsigned char inBytes[FREAD_BUFFER_LENTH];
    unsigned char outBytes[FREAD_BUFFER_LENTH + EVP_MAX_IV_LENGTH];

    /* Sifrovani */
    res = EVP_DecryptInit_ex(ctx, cipher, NULL, key, iv);  // context init - set cipher, key, init vector
    if(res != 1) exit(13);

    while ((bytesRead = fread(inBytes, sizeof(unsigned char), FREAD_BUFFER_LENTH, inFile)) > 0) {
        res = EVP_DecryptUpdate(ctx,  outBytes, &tmpLength, inBytes, bytesRead);  // encryption of pt
        if(res != 1) exit(14);

        bytesWritten = fwrite(outBytes, sizeof(unsigned char), tmpLength, outFile);
        if (tmpLength && !bytesWritten) exit(15);

        newSize += bytesWritten;
    }
    // check if it is the end of the file
    if (!feof(inFile)) exit(16);

    res = EVP_DecryptFinal_ex(ctx, outBytes, &tmpLength);  // get the remaining ct
    if(res != 1) exit(17);

    // write the necessary buffer at the end of file
    bytesWritten = fwrite(outBytes, sizeof(unsigned char), tmpLength, outFile);
    if (tmpLength && !bytesWritten) exit(18);

    newSize += bytesWritten;

    //set the stream pointer 2 bytes from the start.
    fseek(outFile, 2, SEEK_SET);
    fwrite(&newSize, sizeof(uint32_t), 1, outFile);

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    exit(0);
}
