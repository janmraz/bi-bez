#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

int main(int argc, char *argv[]){

    int res;
    char hashFunction[] = "sha256";  // zvolena hashovaci funkce ("sha1", "md5" ...)

    EVP_MD_CTX *ctx;  // struktura kontextu
    const EVP_MD *type; // typ pouzite hashovaci funkce
    unsigned char hash[EVP_MAX_MD_SIZE]; // char pole pro hash - 64 bytu (max pro sha 512)
    int length;  // vysledna delka hashe

    /* Inicializace OpenSSL hash funkci */
    OpenSSL_add_all_digests();
    /* Zjisteni, jaka hashovaci funkce ma byt pouzita */
    type = EVP_get_digestbyname(hashFunction);

    /* Pokud predchozi prirazeni vratilo -1, tak nebyla zadana spravne hashovaci funkce */
    if(!type) {
        printf("Hash %s neexistuje.\n", hashFunction);
        exit(1);
    }

    ctx = EVP_MD_CTX_create(); // create context for hashing
    if(ctx == NULL) exit(2);

    char str[5];

    char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int totalSize = sizeof(alphabet)* sizeof(alphabet)*sizeof(alphabet)*sizeof(alphabet);
    for(int i = 0; i < totalSize; i++){
        str[0] = 'A' + i / (26*26*26) % 26;
        str[1] = 'A' + i / (26*26) % 26;
        str[2] = 'A' + i / (26) % 26;
        str[3] = 'A' + i  % 26;
        str[4] = '\0';

        res = EVP_DigestInit_ex(ctx, type, NULL); // context setup for our hash type
        if(res != 1) exit(3);
        res = EVP_DigestUpdate(ctx, str, strlen(str)); // feed the message in
        if(res != 1) exit(4);
        res = EVP_DigestFinal_ex(ctx, hash, (unsigned int *) &length); // get the hash
        if(res != 1) exit(5);

        if(hash[0] == 0xAA && hash[1] == 0xBB) {
            printf("Hash textu \"%s\" je ",str);
            for(i = 0; i < length; i++){
                printf("%02x", hash[i]);
            }
            printf("\n");
            printf("Text \"%s\" v hexadecimalni podobe je ", str);
            for (int i = 0; i < sizeof str - 1; i ++) {
                printf(" %02x", str[i]);
            }
            putchar('\n');
            break;
        }
    }

    EVP_MD_CTX_destroy(ctx); // destroy the context

    exit(0);
}