#include <stdlib.h>
#include <openssl/evp.h>
#include <string.h>

int main(void) {
    const int LENGTH = 30;
    int res;

    unsigned char OT[1024] = "SuperTajnaZpravaSuperTajnaMsg.";  // secret open text
    unsigned char publicOT[1024] = "abcdefghijklmnopqrstuvwxyz0123";  // public open text

    unsigned char ST[1024];  // sifrovany text
    unsigned char publicST[1024];  // sifrovany text

    unsigned char key[EVP_MAX_KEY_LENGTH] = "SomeSuperDuperKey";  // klic pro sifrovani
    unsigned char iv[EVP_MAX_IV_LENGTH] = "";  // inicializacni vektor
    const char cipherName[] = "RC4";
    const EVP_CIPHER * cipher;

    OpenSSL_add_all_ciphers();
    /* sifry i hashe by se nahraly pomoci OpenSSL_add_all_algorithms() */
    cipher = EVP_get_cipherbyname(cipherName);
    if(!cipher) {
        printf("Sifra %s neexistuje.\n", cipherName);
        exit(1);
    }

    EVP_CIPHER_CTX *ctx; // context structure


    int publicOTLength = strlen((const char*) publicOT);
    int publicSTLength = 0;
    int tmpLength = 0;

    /* Sifrovani */
    ctx = EVP_CIPHER_CTX_new();
    if (ctx == NULL) exit(2);
    res = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv);  // context init - set cipher, key, init vector
    if(res != 1) exit(3);
    res = EVP_EncryptUpdate(ctx, publicST , &tmpLength,publicOT,publicOTLength);  // encryption of pt
    if(res != 1) exit(4);
    publicSTLength += tmpLength;
    res = EVP_EncryptFinal_ex(ctx, publicST + publicSTLength, &tmpLength);  // get the remaining ct
    if(res != 1) exit(5);
    publicSTLength += tmpLength;

    /* Vypsani zasifrovaneho a rozsifrovaneho textu. */
    printf("ST: ");
    for(int i = 0; i < LENGTH; i++)
        printf("%02hhx", publicST[i]);
    printf("\n");
    printf("OT: ");
    for(int i = 0; i < LENGTH; i++)
        printf("%02hhx", publicOT[i]);
    printf("\n");

    int OTLength = strlen((const char*) OT);
    int STLength = 0;

    res = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv);  // context init - set cipher, key, init vector
    if(res != 1) exit(3);
    res = EVP_EncryptUpdate(ctx,  ST, &tmpLength, OT, OTLength);  // encryption of pt
    if(res != 1) exit(4);
    STLength += tmpLength;
    res = EVP_EncryptFinal_ex(ctx, ST + STLength, &tmpLength);  // get the remaining ct
    if(res != 1) exit(5);
    STLength += tmpLength;

    printf("ST2: ");
    for(int i = 0; i < LENGTH; i++)
        printf("%02hhx", ST[i]);
    printf("\n");


    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    exit(0);
}