#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>

int main(int argc, char * argv[])
{

    // --- create SSL connection ---make
    int sockfd;
    struct sockaddr_in servaddr;

    sockfd=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

    memset(&servaddr ,0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr=inet_addr("147.32.232.212"); //ip address
    servaddr.sin_port=htons(443); // port
    // Pozor, nektere platformy maji jeste pole sin_len.

    if( connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) != 0 ) exit(1);

    // initalize SSL
    SSL_library_init();

    // create ssl context
    SSL_CTX * ssl_ctx = SSL_CTX_new(SSLv23_client_method());
    if(!ssl_ctx) exit(2);

    // set where to find certificates by default
    if(SSL_CTX_set_default_verify_paths(ssl_ctx) <= 0) exit(2);

    // disallow unsupported protocols
    SSL_CTX_set_options(ssl_ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);

    // create new SSL structure
    SSL * ssl = SSL_new(ssl_ctx);
    if(!ssl) exit(3);

    // assign open connection
    if( !SSL_set_fd(ssl, sockfd) ) exit(4);

    if(!SSL_set_tlsext_host_name(ssl, "fit.cvut.cz")) exit(5);

    // forbid the cipher used - ignore TLS_AES_256_GCM_SHA384 by not mentioning in the list
    if(SSL_set_ciphersuites(ssl,"TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256") <= 0) exit(6);

    // init communication
    if( SSL_connect(ssl) <= 0 ) exit(7);

    const SSL_CIPHER * sc = SSL_get_current_cipher(ssl);
    const char * cipherName = SSL_CIPHER_get_name(sc);
    printf("Used (original/ignored one) cipher: %s\n", "TLS_AES_256_GCM_SHA384");
    printf("Used (original/ignored one) cipher: %s\n", "TLS_AES_256_GCM_SHA384");
    printf("TLS - security protocol used for encrypting the communication\n");
    printf("AES_256 - symmetric block cipher with 256 bit key length\n");
    printf("GCM - operation mod of AES cipher\n");
    printf("SHA384 - hash method\n\n");

    printf("Used (new one) cipher: %s\n", cipherName);
    printf("TLS - security protocol used for encrypting the communication\n");
    printf("CHACHA20 - stream cipher\n");
    printf("POLY1305 - cryptographic message authethentication code used to check validity and authenticity \n");
    printf("SHA256 - hash method\n\n");


    // list all the ciphers that are available
    printf("All ciphers available:\n");
    const char * cipher;
    int priority = 0;
    cipher = SSL_get_cipher_list(ssl, priority);
    while(cipher)
    {
        printf("%s\n",cipher);
        priority++;
        cipher = SSL_get_cipher_list(ssl,priority);
    }

    // obtain certificate
    X509 * cert = SSL_get_peer_certificate(ssl);
    if(!cert) exit(9);

    // check validity of x509 certificate
    if(SSL_get_verify_result(ssl) != X509_V_OK) exit(10);

    // obtain and check dates
    time_t *ptime;
    int i;
    i=X509_cmp_time(X509_get_notBefore(cert), ptime);
    if(i > -1) exit(7);
    i=X509_cmp_time(X509_get_notAfter(cert), ptime);
    if(i < 1) exit(8);

    // write certificate into file
    FILE * pemFile = fopen("cert.pem","w");
    if( !PEM_write_X509(pemFile, cert) ) exit(10);

    // close writing certificate
    fclose(pemFile);

    // create buffer to send and receive batches
    char buffer[1024] = "GET /cs/studium/informacni-servis/rozcestnik-systemu HTTP/1.1\r\nHost: fit.cvut.cz\r\nConnection: close\r\n\r\n";

    // send http request
    if( SSL_write(ssl, buffer, strlen(buffer) + 1) <= 0 ) exit(11);

    int readBytes;
    FILE * outputFile = fopen("out.html","w");

    while( (readBytes = SSL_read(ssl, buffer, 1024)) > 0 )
    {
        // write batch into file
        fwrite(buffer, sizeof(char), readBytes, outputFile);
    }

    // write output into file
    fprintf(outputFile, "\n");

    // close fd into file
    fclose(outputFile);

    // close SSL session
    SSL_shutdown(ssl);

    // close SSL connection
    close(sockfd);

    // Release SSL structure
    SSL_free(ssl);

    // Release SSL context
    SSL_CTX_free(ssl_ctx);

    return 0;
}